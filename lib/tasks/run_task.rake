require_relative File.expand_path("../../pptp_task", __FILE__)
require_relative File.expand_path("../../vps_task", __FILE__)
require_relative File.expand_path("../../fast_vps_task", __FILE__)
require_relative File.expand_path("../../qg_imp_proxy_task", __FILE__)
require_relative File.expand_path("../../pptp_task_backup", __FILE__)
ENV['DISPLAY'] = ":1"

desc "run data_agent pptp_task process"
task :run_pptp_task do
  PptpTask.run
end

desc "run data_agent vps_task process"
task :run_vps_task do
  VpsTask.run
end

desc "run data_agent fast_vps_task process"
task :run_fast_vps_task do
  FastVpsTask.run
end

desc "run data_agent qg_imp_proxy_task process"
task :run_qg_imp_proxy_task do
  QgImpProxyTask.run
end

desc "run data_agent pptp_task_backup process"
task :run_pptp_task_backup do
  PptpTaskBackup.run
end