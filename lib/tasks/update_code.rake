require_relative File.expand_path("../../update_code", __FILE__)
ENV['DISPLAY'] = ":1"
desc "run update code process"
task :run_update_code do
  UpdateCode.run
end

desc "run update code for fast_vps mode"
task :exec_update_code do
  UpdateCode.run_for_fast_vps
end