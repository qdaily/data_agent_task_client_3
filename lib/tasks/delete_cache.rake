require_relative File.expand_path("../../delete_cache", __FILE__)
ENV['DISPLAY'] = ":1"
desc "run delete cache process"
task :run_delete_cache do
  DeleteCache.run
end