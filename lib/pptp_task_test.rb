#encoding: utf-8
# ruby lib/pptp_task_test.rb task_id

require 'rubygems'
require 'bundler/setup'
Bundler.require
require_relative 'settings'
require_relative 'tools'
require_relative 'click'
require_relative 'impression'

class PptpTaskTest
  @@logger = Logger.new(File.expand_path('../../log/pptp_task_test.log', __FILE__), 3, 10485760)

  def self.run(task_id = nil)
    pptp_task_test = PptpTaskTest.new

    begin
      # 1. 获取任务内容（ok）
      @@logger.warn "开始获取任务内容"
      get_task_result = pptp_task_test.get_task_by_id(task_id)
      return @@logger.warn "获取任务内容失败".red unless get_task_result["tasks"]
      task = get_task_result["tasks"].first

      # 2. 执行任务（ok）
      device_id_status = true
      if task["task_type"] == "click"#点击任务
        @@logger.warn "开始执行点击任务"
        Click.click(task, @@logger)
        @@logger.warn "执行点击任务成功".green
      elsif task["task_type"] == "impression"#曝光任务
        @@logger.warn "开始执行曝光任务"
        Impression.impression(task, @@logger)
        @@logger.warn "执行曝光任务成功".green
      elsif task["task_type"] == "click_with_device_id"#点击任务（带设备号）
        @@logger.warn "开始执行点击任务（带设备号）"
        device_id_status = Click.click(task, @@logger, true)
        @@logger.warn "执行点击任务（带设备号）#{device_id_status ? '成功' : '失败'}".green
      elsif task["task_type"] == "impression_with_device_id"#曝光任务（带设备号）
        @@logger.warn "开始执行曝光任务（带设备号）"
        device_id_status = Impression.impression(task, @@logger, true)
        @@logger.warn "执行曝光任务（带设备号）#{device_id_status ? '成功' : '失败'}".green
      else
      end
    rescue => e
      `killall chrome`
      @@logger.warn "任务失败：#{e}".red
    end
  end

  def get_task_by_id(task_id)
    result = {}
    begin
      Timeout.timeout(10, Errno::ETIMEDOUT) do
        url = File.join(Settings.defaults.data_agent, "/api/tasks/#{task_id}?city_title=全国混合")
        url = Addressable::URI.parse(url).normalize
        result = JSON.parse(open(url).read)
      end
    rescue => e
      @@logger.warn "获取任务失败：#{e}".red
    end
    return result
  end

end

PptpTaskTest.run(ARGV[0])
