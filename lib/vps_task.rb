#encoding: utf-8
require 'rubygems'
require 'bundler/setup'
Bundler.require
require_relative 'settings'
require_relative 'tools'
require_relative 'click'
require_relative 'impression'

class VpsTask
  @@logger = Logger.new(File.expand_path('../../log/vps_task.log', __FILE__), 3, 10485760)
  PATH = File.dirname(Pathname.new(__FILE__).realpath.to_path)
  VERSION = "1.2.0"

  def self.run
    count = 5
    vps_task = VpsTask.new
    server_title = Socket.gethostname
    token = Digest::MD5::hexdigest("data_agent" + Time.now.strftime("%Y-%m-%d-%H"))

    # 1. 检测系统性能（ok）
    @@logger.warn "开始检测系统负载"
    return @@logger.warn "系统负载已满".red if Tools.check_performance
    @@logger.warn "系统负载检测通过".green

    # 2. 检测唯一进程（ok）
    @@logger.warn "开始检查是否是唯一进程"
    process_count = `ps aux|grep run_vps_task|grep ruby |wc -l`
    @@logger.warn "当前进程数：#{process_count}"
    return @@logger.warn "不是唯一进程".red if process_count.to_i > 2
    @@logger.warn "唯一进程检测通过".green

    count.times do |i|
      begin
        sleep 10 if i > 0
        @@logger.warn "开始第#{i+1}次任务"

        # 3. 获取账户（ok）
        @@logger.warn "开始获取账户"
        get_account_result = vps_task.get_account(server_title, token, VERSION)
        if get_account_result["state"] == false
          @@logger.warn "获取账户失败".red
          next
        end
        proxy_host = get_account_result.dig("account", "url") + ":9928"
        proxy_username = get_account_result.dig("account", "username")
        proxy_password = get_account_result.dig("account", "password")
        vps_account_id = get_account_result.dig("account", "account_id")
        @@logger.warn "获取账户成功".green

        # 4. 检测ip
        @@logger.warn "开始检测ip"
        check_ip_result = Tools.check_ip(@@logger, server_title, token, proxy_host, proxy_username, proxy_password)
        if check_ip_result == false
          @@logger.warn "检测ip失败".red
          vps_task.update_account(vps_account_id, "needupdate", token, VERSION)#修改proxy状态
          next
        end
        @@logger.warn "检测ip成功".green

        Tools.check_performance_and_wait
        # 5. 检测网速
        @@logger.warn "开始检测网速"
        check_network_result = Tools.check_network(@@logger, proxy_host, proxy_username, proxy_password)
        if check_network_result == false
          @@logger.warn "检测网速失败".red
          vps_task.update_account(vps_account_id, "needupdate", token, VERSION)#修改proxy状态
          next
        end
        @@logger.warn "检测网速成功".green

        # 6. 获取任务内容
        @@logger.warn "开始获取任务内容"
        get_task_result = vps_task.get_task(server_title, get_account_result["account"]["city_title"], token, VERSION)
        @@logger.warn "获取任务内容成功，任务数量：#{get_task_result["tasks"].count}".green

        get_task_result["tasks"].each do |task|
          Tools.check_performance_and_wait
          # 7. 执行任务
          device_id_status = true
          if task["task_type"] == "click"#点击任务
            @@logger.warn "开始执行点击任务"
            Click.click(task, @@logger, false, 0, proxy_host, proxy_username, proxy_password)
            @@logger.warn "执行点击任务成功".green
          elsif task["task_type"] == "impression"#曝光任务
            @@logger.warn "开始执行曝光任务"
            Impression.impression(task, @@logger, false, proxy_host, proxy_username, proxy_password)
            @@logger.warn "执行曝光任务成功".green
          elsif task["task_type"] == "click_with_device_id"#点击任务（带设备号）
            @@logger.warn "开始执行点击任务（带设备号）"
            device_id_status = Click.click(task, @@logger, true, 0, proxy_host, proxy_username, proxy_password)
            @@logger.warn "执行点击任务（带设备号）#{device_id_status ? '成功' : '失败'}".green
          elsif task["task_type"] == "impression_with_device_id"#曝光任务（带设备号）
            @@logger.warn "开始执行曝光任务（带设备号）"
            device_id_status = Impression.impression(task, @@logger, true, proxy_host, proxy_username, proxy_password)
            @@logger.warn "执行曝光任务（带设备号）#{device_id_status ? '成功' : '失败'}".green
          else
            next
          end
          next unless device_id_status

          # 8. 上传任务结果（ok）
          @@logger.warn "开始上传任务结果"
          statistic_url = File.join(Settings.defaults.data_agent, "/api/visits/statistic_visit?task_id=#{task["id"]}&account_id=#{get_account_result["account"]["account_id"]}&token=#{token}")
          open(statistic_url, :proxy_http_basic_authentication => [URI.parse("http://#{proxy_host}"), proxy_username, proxy_password], "User-Agent" => task["useragent"]["content"])
          @@logger.warn "上传任务结果成功".green

        end

        # 9. 将账号更新为已使用（ok）
        vps_task.update_account(vps_account_id, "needupdate", token, VERSION)
        @@logger.warn "第#{i+1}次任务完成".green
      rescue => e
        `killall chrome`
        vps_task.update_account(vps_account_id, "needupdate", token, VERSION)
        @@logger.warn "第#{i+1}次任务失败：#{e}".red
      end
    end

  end

  def get_account(server_title, token, version)
    result = {"state" => false}
    begin
      Timeout.timeout(10, Errno::ETIMEDOUT) do
        url = File.join(Settings.defaults.data_agent, "/api/vps_accounts/show_by_task_client_title?task_client_title=#{server_title}&task_client_version=#{version}")
        result = JSON.parse(open(url).read)
      end
    rescue => e
      @@logger.warn "获取账户失败：#{e}".red
    end
    return result
  end

  def update_account(id, status, token, version)
    res = Tools.http_request method: :post, url: File.join(Settings.defaults.data_agent, "/api/vps_accounts/update_proxy_client_status"), params: {account_id: id, proxy_client_status: status}
  end

  def get_task(server_title, city_title, token, version)
    result = {}
    begin
      Timeout.timeout(10, Errno::ETIMEDOUT) do
        url = File.join(Settings.defaults.data_agent, "/api/tasks?city_title=#{city_title}&token=#{token}&account_type=vps")
        url = Addressable::URI.parse(url).normalize
        result = JSON.parse(open(url).read)
      end
    rescue => e
      @@logger.warn "获取任务失败：#{e}".red
    end
    return result
  end

end
