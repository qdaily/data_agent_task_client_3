#encoding: utf-8
require 'rubygems'
require 'bundler/setup'
Bundler.require
require_relative 'settings'
require_relative 'tools'

$redis = Redis.new(Settings.defaults.redis.to_h)

class UpdateCode

  def self.run
    # 1. 随机等待0～1分钟
    sleep rand(1..60)

    # 2. 更新代码
    for m in 1..10
      success = false
      begin
        Timeout.timeout(20, Errno::ETIMEDOUT) do
          `sudo git pull`
          success = true
        end
      rescue => e
        sleep 5
      end
      break if success
    end
  end

  def self.run_for_fast_vps
    # 1. 随机等待0～1分钟
    sleep rand(1..60)

    # 2. 暂停任务执行
    pause_key = "fast_vps:pause_for_update"
    $redis.set(pause_key, 1)
    $redis.expire(pause_key, 60)

    # 3. 更新代码
    for m in 1..5
      success = false
      begin
        Timeout.timeout(20, Errno::ETIMEDOUT) do
          `git pull`
          success = true
          $redis.del pause_key
        end
      rescue => e
        sleep 5
      end
      break if success
    end
    
    $redis.del(pause_key)
  end

end
