#encoding: utf-8
require 'rubygems'
require 'bundler/setup'
Bundler.require
require_relative 'settings'
require_relative 'tools'
require_relative 'impression'

$redis = Redis.new(Settings.defaults.redis.to_h)

class QgImpProxyTask
  @@logger = Logger.new(File.expand_path('../../log/qg_imp_proxy_task.log', __FILE__), 3, 10485760)
  PATH = File.dirname(Pathname.new(__FILE__).realpath.to_path)
  VERSION = "1.2.0"

  def self.run
    count = 30
    qg_imp_proxy_task = QgImpProxyTask.new
    server_title = Socket.gethostname
    token = Digest::MD5::hexdigest("data_agent" + Time.now.strftime("%Y-%m-%d-%H"))

    # 1. 检测系统性能（ok）
    @@logger.warn "开始检测系统负载"
    return @@logger.warn "系统负载已满".red if Tools.check_performance
    @@logger.warn "系统负载检测通过".green

    count.times do |i|
      begin
        @@logger.warn "开始第#{i+1}次任务"

        # 2. 获取账户（ok）
        @@logger.warn "开始获取账户"
        get_account_result = qg_imp_proxy_task.get_account(server_title, token, VERSION)
        next @@logger.warn "获取账户失败".red if get_account_result["state"] == false
        proxy_username = get_account_result.dig("account", "username")
        proxy_password = get_account_result.dig("account", "password")
        @@logger.warn "获取账户成功".green

        # 3. 检查任务数量
        @@logger.warn "开始检查任务数量"
        task_count = qg_imp_proxy_task.get_task_count(server_title, get_account_result["account"]["city_title"], token, VERSION)
        next (@@logger.warn("任务数量为0".red) && sleep(5)) if task_count == 0
        @@logger.warn "检查任务数量成功".green

        # 4. 释放过期通道
        @@logger.warn "开始释放过期通道"
        qg_imp_proxy_task.release_expired_quota(proxy_username)
        @@logger.warn "释放过期通道成功".green

        # 5. 检查可用通道
        next @@logger.warn "无可用通道".red unless qg_imp_proxy_task.check_quota(proxy_username)

        # 6. 申请ip
        @@logger.warn "开始申请ip"
        qg_task_id, proxy_host = qg_imp_proxy_task.apply_ip(proxy_username)
        next @@logger.warn "申请ip失败".red unless qg_task_id
        @@logger.warn "申请ip成功".green

        # 7. 检测ip
        @@logger.warn "开始检测ip"
        check_ip_result = Tools.check_ip(@@logger, server_title, token, proxy_host, proxy_username, proxy_password)
        if check_ip_result == false
          @@logger.warn "检测ip失败 - #{proxy_host}".red
          # 释放ip
          qg_imp_proxy_task.release_quota(proxy_username, qg_task_id)
          next
        end
        @@logger.warn "检测ip成功".green

        Tools.check_performance_and_wait

        # 8. 获取任务内容
        @@logger.warn "开始获取任务内容"
        get_task_result = qg_imp_proxy_task.get_task(server_title, get_account_result["account"]["city_title"], token, VERSION)
        @@logger.warn "获取任务内容成功，任务数量：#{get_task_result["tasks"].count}".green

        get_task_result["tasks"].each do |task|
          Tools.check_performance_and_wait
          # 9. 执行任务
          device_id_status = true
          if task["task_type"] == "impression"#曝光任务
            @@logger.warn "开始执行曝光任务"
            device_id_status = Impression.impression(task, @@logger, false, proxy_host, proxy_username, proxy_password)
            @@logger.warn "执行曝光任务#{device_id_status ? '成功' : '失败'}".green
          elsif task["task_type"] == "impression_with_device_id"#曝光任务（带设备号）
            @@logger.warn "开始执行曝光任务（带设备号）"
            device_id_status = Impression.impression(task, @@logger, true, proxy_host, proxy_username, proxy_password)
            @@logger.warn "执行曝光任务（带设备号）#{device_id_status ? '成功' : '失败'}".green
          else
            next
          end
          next unless device_id_status
        end

        # 10. 上传任务结果
        @@logger.warn "开始上传任务结果"
        visits = get_task_result["tasks"].collect{|task| {task_id: task["id"], useragent: task["useragent"]["content"], account_id: get_account_result["account"]["account_id"]}}
        qg_imp_proxy_task.create_visits(proxy_host, proxy_username, proxy_password, visits.to_json, token)
        @@logger.warn "上传任务结果成功!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!".green

        # 11. 释放ip
        qg_imp_proxy_task.release_quota(proxy_username, qg_task_id)
        @@logger.warn "第#{i+1}次任务完成".green
      rescue => e
        qg_imp_proxy_task.release_quota(proxy_username, qg_task_id)
        @@logger.warn "第#{i+1}次任务失败：#{e}".red
        @@logger.warn "第#{i+1}次任务失败：#{e.backtrace.join('\n')}".red
      end
    end
  end

  def get_account(server_title, token, version)
    result = {"state" => false}
    begin
      Timeout.timeout(10, Errno::ETIMEDOUT) do
        url = File.join(Settings.defaults.data_agent, "/api/qg_imp_proxy_accounts/show_by_task_client_title?task_client_title=#{server_title}&task_client_version=#{version}")
        result = JSON.parse(open(url).read)
      end
    rescue => e
      @@logger.warn "获取账户失败：#{e}".red
    end
    return result
  end

  def get_task_count(server_title, city_title, token, version)
    result = {}
    begin
      Timeout.timeout(10, Errno::ETIMEDOUT) do
        url = File.join(Settings.defaults.data_agent, "/api/tasks/show_task_count?&city_title=#{city_title}&token=#{token}&account_type=qg_imp_proxy")
        url = Addressable::URI.parse(url).normalize
        result = JSON.parse(open(url).read, symbolize_names: true)
      end
    rescue => e
      @@logger.warn "获取任务失败：#{e}".red
    end
    return result[:task_count].to_i
  end

  def release_expired_quota(app_key)
    url = "https://proxy.qg.net/query"
    qg_task_ids = []

    params = {
      "Key" => app_key,
    }
    res = http_request(method: :post, url: url, params: params) || {}
    
    (res.dig(:TaskList) || []).each do |item|
      task_id = item.dig(:TaskID)
      next unless task_id
      task_id_key = "qg_task:#{task_id}"

      if $redis.get(task_id_key)
        qg_task_ids << task_id if (Time.now.to_i - $redis.get(task_id_key).to_i > 20)
      else
        $redis.set(task_id_key, Time.now.to_i)
        $redis.expire(task_id_key, 60*10)
      end
    end

    @@logger.warn "#{qg_task_ids.size} 个通道 #{qg_task_ids.join(',')} 将被释放" if qg_task_ids.size > 0
    release_quota(app_key, qg_task_ids.join(',')) if qg_task_ids.size > 0
  end

  def get_task(server_title, city_title, token, version)
    result = {}
    begin
      Timeout.timeout(10, Errno::ETIMEDOUT) do
        url = File.join(Settings.defaults.data_agent, "/api/tasks?city_title=#{city_title}&token=#{token}&account_type=qg_imp_proxy")
        url = Addressable::URI.parse(url).normalize
        result = JSON.parse(open(url).read)
      end
    rescue => e
      @@logger.warn "获取任务失败：#{e}".red
      @@logger.warn "获取任务失败：#{e.backtrace.join('\n')}".red
    end
    return result
  end

  def check_quota(app_key)
    url = "https://proxy.qg.net/info/quota"
    params = {
      "Key" => app_key,
    }
    res = http_request(method: :post, url: url, params: params) || {}
    @@logger.info "通道：#{res.dig(:Available).to_i}".green
    return res.dig(:Available).to_i > 0
  end

  def apply_ip(app_key)
    url = "https://proxy.qg.net/allocate"
    params = {
      "Key" => app_key,
      # "AreaId" => "1,3",
      "Num" => 1,
      "detail" => 1
    }
    res = http_request(method: :post, url: url, params: params)
    begin
      if res.dig(:Code) == 0
        return res.dig(:TaskID), res.dig(:Data).first.dig(:host)
      else
        return false, false
      end
    rescue => e
      @@logger.error e.message
      @@logger.error url
      @@logger.error res
      return false, false
    end
  end

  def release_quota(app_key, qg_task_id)
    url = "https://proxy.qg.net/release"
    params = {
      "Key" => app_key,
      "TaskID" => qg_task_id, # 任务ID，多个以逗号分割，*代表全部
    }
    @@logger.warn "释放：#{qg_task_id}".green
    res = http_request(method: :post, url: url, params: params) || {}
    return res.dig(:Code) == 0
  end

  def http_request(method: :get, url: , params: {})
    begin
      Timeout.timeout(5, Errno::ETIMEDOUT) do
        http_res = RestClient::Request.execute(method: method, url: url, payload: params, timeout: 60)
        return JSON.parse(http_res.body, :symbolize_names => true )
      end
    rescue => e
      i ||= 0
      i += 1
      retry if i < 3
      @@logger.warn e.message.red
      @@logger.warn e.backtrace.join("\n")
      nil
    end
  end

  def create_visits(proxy_host, proxy_username, proxy_password, visits, token)
    url = File.join(Settings.defaults.data_agent, "/api/visits")
    uri = URI.parse(url)
    begin
      Timeout.timeout(10, Errno::ETIMEDOUT) do
        proxy = Net::HTTP::Proxy(proxy_host.split(":").first, proxy_host.split(":").last, proxy_username, proxy_password)
        proxy.post_form(uri, visits: visits, token: token).body
      end
    rescue => e
      @@logger.error "创建访问记录失败：#{e}".red
    end
  end

end
