#encoding: utf-8
class Tools

  def self.check_performance
    result = (Vmstat.snapshot.cpus.count - Vmstat.snapshot.load_average.one_minute <= 0.3)
  end

  def self.check_performance_and_wait
    100.times do |i|
      if Tools.check_performance
        sleep 3
      else
        break
      end
    end
  end

  def self.http_request(method: :get, url: , params: {})
    begin
      http_res = RestClient::Request.execute(method: method, url: url, payload: params, timeout: 60)
      JSON.parse(http_res.body, :symbolize_names => true )
    rescue
      i ||= 0
      i += 1
      retry if i < 3
      nil
    end
  end

  def self.check_ip(logger, server_title, token, proxy_host = nil, proxy_username = nil, proxy_password = nil)
    result = false
    begin
      Timeout.timeout(10, Errno::ETIMEDOUT) do
        url = File.join(Settings.defaults.data_agent, "/api/tasks/check_ip?token=#{token}&task_client_title=#{server_title}")
        result = JSON.parse(proxy_host ? open(url, proxy_http_basic_authentication: [URI.parse("http://#{proxy_host}"), proxy_username, proxy_password]).read : open(url).read)["state"]
      end
    rescue => e
      logger.warn "检测ip失败：#{e}".red
    end
    return result
  end

  def self.get_real_ip(proxy_host = nil, proxy_username = nil, proxy_password = nil)
    result = Timeout.timeout(proxy_host ? 10 : 5, Errno::ETIMEDOUT) do
      url = File.join(Settings.defaults.data_agent, "real_ip")
      proxy_host ? open(url, proxy_http_basic_authentication: [URI.parse("http://#{proxy_host}"), proxy_username, proxy_password]).read : open(url).read
    end rescue ""
    return result =~ /^\d+\.\d+\.\d+\.\d+$/ ? result : ""
  end

  def self.check_network(logger, proxy_host=nil, proxy_username=nil, proxy_password=nil)
    result = false
    if proxy_host
      options = Selenium::WebDriver::Chrome::Options.new
      options.add_argument("--proxy-server=#{proxy_host}")
      options.add_argument("homepage=http://img.somecoding.com/1k.jpg")
      driver = Selenium::WebDriver.for(:chrome, options: options)
      browser = Watir::Browser.new driver
    else
      browser = Watir::Browser.new :chrome
    end
    begin
      Timeout.timeout(proxy_host ? 25 : 20, Errno::ETIMEDOUT) do
        Tools.goto_with_auth(proxy_username, proxy_password) if proxy_host
        browser.goto "http://img.somecoding.com/1000k.html"
        result = true if browser.html.include?(">完成<")
      end
    rescue => e
      logger.warn "检测网速失败：#{e}".red
    ensure
      `killall chrome`
    end
    return result
  end

  def self.check_process_exist(process_file)
    if File.exists?(process_file)
      count = Tools.get_count(process_file).to_i
      if count > 30
        Tools.set_count(process_file)
        result = false
      else
        Tools.set_count(process_file, count+1)
        result = true
      end
    else
      %x{touch #{process_file}}
      Tools.set_count(process_file)
      result = false
    end
    return result
  end

  def self.delete_process_file(process_file)
    File.delete(process_file) if File.exists?(process_file)
  end

  def self.browser_js(browser, content)
    begin
      Timeout.timeout(rand(14..16), Errno::ETIMEDOUT) do
        browser.execute_script content#执行js
      end
    rescue => e
    end
  end

  def self.mouse_click(window_id, x_begin, y_begin, x_radius, y_radius)
    x_offset_set = []
    for i in 0..x_radius
      for j in 1..(x_radius+1-i)
        x_offset_set << i
      end
    end
    x_offset = x_offset_set.shuffle[0]
    x = [x_begin - x_offset, x_begin + x_offset].shuffle[0]

    y_offset_set = []
    for i in 0..y_radius
      for j in 1..(y_radius+1-i)
        y_offset_set << i
      end
    end
    y_offset = y_offset_set.shuffle[0]
    y = [y_begin - y_offset, y_begin + y_offset].shuffle[0]

    `xdotool mousemove --window #{window_id} #{x} #{y} click 1`
  end


  def self.quick_click_with_random_sleep(window_id, x_begin, y_begin, x_radius, y_radius, repeat_times=1)
    (repeat_times + rand(-5..5)).times do
      x_offset_set = []
      for i in 0..x_radius
        for j in 1..(x_radius+1-i)
          x_offset_set << i
        end
      end
      x_offset = x_offset_set.shuffle[0]
      x = [x_begin - x_offset, x_begin + x_offset].shuffle[0]

      y_offset_set = []
      for i in 0..y_radius
        for j in 1..(y_radius+1-i)
          y_offset_set << i
        end
      end
      y_offset = y_offset_set.shuffle[0]
      y = [y_begin - y_offset, y_begin + y_offset].shuffle[0]

      `xdotool mousemove --window #{window_id} #{x} #{y} click 1`
      sleep rand(1..3)/10.0
    end
  end



  def self.mouse_move(window_id, x_begin, y_begin, x_end, y_end)
    `xdotool mousemove --window #{window_id} #{x_begin} #{y_begin} mousedown 1 sleep 0.5 mousemove #{x_end} #{y_end} mouseup 1`
  end

  def self.keyboard_down(click_count)
    for i in 1..click_count
      `xdotool key 104`
      sleep 0.5
    end
  end

  def self.mouse_move_without_sleep(window_id, x_begin, y_begin, x_end, y_end)
    `xdotool mousemove --window #{window_id} #{x_begin} #{y_begin} mousedown 1 mousemove #{x_end} #{y_end} mouseup 1`
  end

  def self.goto_with_auth(proxy_username, proxy_password)
    `xdotool key 64+97`  #ALT + HOME
    sleep 3
    `xdotool type #{proxy_username}`
    `xdotool key 23`     #TAB
    `xdotool type #{proxy_password}`
    `xdotool key 36`     #RETURN
    sleep 1
  end

  private

  def self.get_count(process_file)
      File.open(process_file, "r") {|f|
        f.flock(File::LOCK_SH)
        number = f.read
      }
  end

  def self.set_count(process_file, number=1)
      File.open(process_file, File::RDWR|File::CREAT, 0644) {|f|
        f.flock(File::LOCK_EX)
        f.rewind
        f.write(number)
        f.flush
        f.truncate(f.pos)
      }
  end

end

