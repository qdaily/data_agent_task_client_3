#encoding: utf-8
require_relative 'tools'

class Impression
  ALL_RESIDENCE_TIME = 100

  def self.impression(task, logger, with_device_id=false, proxy_host=nil, proxy_username=nil, proxy_password=nil)
    #基本参数
    source_url = task["source_url"].split("&&").shuffle[0]
    impression_url = task["impression_url"]
    impression_track_url = task["impression_track_url"]
    impression_rate = task["impression_rate"].to_i + [-1,0,0,0,1].sample
    user_agent = task["useragent"]["content"]
    if with_device_id
      if task["device"]["device_id"].to_s.length < 2 or !%w(iOS android).include?(task["device"]["app_type"])
        logger.warn "设备号不正确".red
        return false
      end
      impression_url = impression_url.gsub(task["device"]["app_type"] == "iOS" ? "__IDFA__" : "__IMEI__", task["device"]["device_id"])
      user_agent = task["device"]["useragent"]
    end

    #open设置
    open_options = {"User-Agent" => user_agent}
    open_options = open_options.merge("Referer" => source_url) if (source_url and !source_url.empty?)
    open_options = open_options.merge(:proxy_http_basic_authentication => [URI.parse("http://#{proxy_host}"), proxy_username, proxy_password]) if proxy_host

    begin
      Timeout.timeout(ALL_RESIDENCE_TIME, Errno::ETIMEDOUT) do
        for m in 1..impression_rate
          open(impression_url, open_options)
          open(impression_track_url, open_options) if impression_track_url and !impression_track_url.empty?
          sleep rand(6..10) unless m == impression_rate
        end
      end
    rescue => e
      logger.warn "执行曝光任务失败：#{e}"
      return false
    end
    return true # device_id is correct
  end
end

