#encoding: utf-8
require 'rubygems'
require 'bundler/setup'
require 'fileutils'
Bundler.require
require_relative 'settings'
require_relative 'tools'

class DeleteCache
  PATH = File.dirname(Pathname.new(__FILE__).realpath.to_path)

  def self.run
    cache_folder = File.join(File.dirname(PATH), "cache")
    FileUtils.rm_r(cache_folder) if File.exists?(cache_folder)
  end
  
end