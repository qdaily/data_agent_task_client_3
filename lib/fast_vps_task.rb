#encoding: utf-8
require 'rubygems'
require 'bundler/setup'
Bundler.require
require_relative 'settings'
require_relative 'tools'
require_relative 'click'
require_relative 'impression'

class FastVpsTask
  @@logger = Logger.new(File.expand_path('../../log/fast_vps_task.log', __FILE__), 3, 10485760)
  PATH = File.dirname(Pathname.new(__FILE__).realpath.to_path)
  VERSION = "1.0.3"

  def self.run
    count = 5
    interval_time = 5
    fast_vps_task = FastVpsTask.new
    server_title = Socket.gethostname
    token = Digest::MD5::hexdigest("data_agent" + Time.now.strftime("%Y-%m-%d-%H"))

    # 1. 检测系统性能（ok）
    @@logger.warn "开始检测系统负载"
    return @@logger.warn "系统负载已满".red if Tools.check_performance
    @@logger.warn "系统负载检测通过".green

    # 2. 检测唯一进程（ok）
    @@logger.warn "开始检查是否是唯一进程"
    process_count = `ps aux|grep run_fast_vps_task|grep ruby | egrep -v 'grep' |wc -l`
    @@logger.warn "当前进程数：#{process_count}"
    return @@logger.warn "不是唯一进程".red if process_count.to_i > 1
    @@logger.warn "唯一进程检测通过".green

    count.times do |i|
      begin
        @@logger.warn "开始第#{i+1}次任务"

        # 3. 如果需要，等待代码更新
        fast_vps_task.pause_for_update

        # 4. 启动vps拨号
        @@logger.warn "开始启动vps拨号"
        begin_time = Time.now.to_i
        change_ip_result = fast_vps_task.change_ip
        if change_ip_result == false
          @@logger.warn "启动vps拨号失败".red
          task_time = Time.now.to_i - begin_time
          sleep (interval_time - task_time) if task_time < interval_time
          next
        end
        @@logger.warn "启动vps拨号成功".green

        # 5. 获取账户（ok）
        @@logger.warn "开始获取账户"
        get_account_result = fast_vps_task.get_account(server_title, token, VERSION)
        next @@logger.warn "获取账户失败".red if get_account_result["state"] == false
        @@logger.warn "获取账户成功".green

        # 6. 检查任务数量
        @@logger.warn "开始检查任务数量"
        task_count = fast_vps_task.get_task_count(server_title, get_account_result["account"]["city_title"], token, VERSION)
        next (@@logger.warn("任务数量为0".red) && sleep(5)) if task_count == 0
        @@logger.warn "检查任务数量成功".green

        # 7. 检测ip（ok）
        @@logger.warn "开始检测ip"
        check_ip_result = Tools.check_ip(@@logger, server_title, token)
        if check_ip_result == false
          @@logger.warn "检测ip失败".red
          task_time = Time.now.to_i - begin_time
          sleep (interval_time - task_time) if task_time < interval_time
          next
        end
        @@logger.warn "检测ip成功".green

        Tools.check_performance_and_wait
        # 8. 检测网速（ok）
        @@logger.warn "开始检测网速"
        check_network_result = Tools.check_network(@@logger)
        if check_network_result == false
          @@logger.warn "检测网速失败".red
          task_time = Time.now.to_i - begin_time
          sleep (interval_time - task_time) if task_time < interval_time
          next
        end
        @@logger.warn "检测网速成功".green

        # 9. 获取任务内容（ok）
        @@logger.warn "开始获取任务内容"
        get_task_result = fast_vps_task.get_task(server_title, get_account_result["account"]["city_title"], token, VERSION)
        next @@logger.warn "获取任务内容失败".red unless get_task_result["tasks"]
        @@logger.warn "获取任务内容成功，任务数量：#{get_task_result["tasks"].count}".green

        get_task_result["tasks"].each do |task|
          Tools.check_performance_and_wait
          # 10. 执行任务（ok）
          device_id_status = true
          if task["task_type"] == "click"#点击任务
            @@logger.warn "开始执行点击任务"
            Click.click(task, @@logger, false, 10)
            @@logger.warn "执行点击任务成功".green
          elsif task["task_type"] == "impression"#曝光任务
            @@logger.warn "开始执行曝光任务"
            Impression.impression(task, @@logger)
            @@logger.warn "执行曝光任务成功".green
          elsif task["task_type"] == "click_with_device_id"#点击任务（带设备号）
            @@logger.warn "开始执行点击任务（带设备号）"
            device_id_status = Click.click(task, @@logger, true, 10)
            @@logger.warn "执行点击任务（带设备号）#{device_id_status ? '成功' : '失败'}".green
          elsif task["task_type"] == "impression_with_device_id"#曝光任务（带设备号）
            @@logger.warn "开始执行曝光任务（带设备号）"
            device_id_status = Impression.impression(task, @@logger, true)
            @@logger.warn "执行曝光任务（带设备号）#{device_id_status ? '成功' : '失败'}".green
          else
            next
          end
          next unless device_id_status
        end

        # 11. 上传任务结果
        @@logger.warn "开始上传任务结果"
        visits = get_task_result["tasks"].collect{|task| {task_id: task["id"], useragent: task["useragent"]["content"], account_id: get_account_result["account"]["account_id"]}}
        fast_vps_task.create_visits(visits.to_json, token)
        @@logger.warn "上传任务结果成功".green

        @@logger.warn "第#{i+1}次任务完成".green
      rescue => e
        `killall chrome`
        @@logger.warn "第#{i+1}次任务失败：#{e}".red
      end
    end
  end

  def get_account(server_title, token, version)#ok
    result = {"state" => false}
    begin
      Timeout.timeout(10, Errno::ETIMEDOUT) do
        url = File.join(Settings.defaults.data_agent, "/api/fast_vps_accounts/show_by_task_client_title?task_client_title=#{server_title}&token=#{token}&task_client_version=#{version}")
        result = JSON.parse(open(url).read)
      end
    rescue => e
      @@logger.warn "获取账户失败：#{e}".red
      result = {"state" => false}
    end
    return result
  end

  def get_task_count(server_title, city_title, token, version)
    result = {}
    begin
      Timeout.timeout(10, Errno::ETIMEDOUT) do
        url = File.join(Settings.defaults.data_agent, "/api/tasks/show_task_count?&city_title=#{city_title}&token=#{token}&account_type=fast_vps")
        url = Addressable::URI.parse(url).normalize
        result = JSON.parse(open(url).read, symbolize_names: true)
      end
    rescue => e
      @@logger.warn "获取任务失败：#{e}".red
    end
    return result[:task_count].to_i
  end

  def get_task(server_title, city_title, token, version)#ok
    result = {}
    begin
      Timeout.timeout(10, Errno::ETIMEDOUT) do
        url = File.join(Settings.defaults.data_agent, "/api/tasks?&city_title=#{city_title}&token=#{token}&account_type=fast_vps")
        url = Addressable::URI.parse(url).normalize
        result = JSON.parse(open(url).read)
      end
    rescue => e
      @@logger.warn "获取任务失败：#{e}".red
    end
    return result
  end

  def change_ip
    result = false
    begin
      Timeout.timeout(30, Errno::ETIMEDOUT) do
        `sudo pppoe-stop`
        sleep 1
        #开启vps拨号
        `sudo pppoe-start`
        20.times do |i|
          if `route -n`.include?("ppp0")
            result = true
            break
          end
          sleep 1
        end
        raise "vps拨号 start failed" unless result
      end
    rescue => e
      @@logger.warn "启动vps拨号失败：#{e}".red
    end
    `sudo pppoe-stop` if result == false
    return result
  end

  def create_visits(visits, token)
    result = nil
    begin
      Timeout.timeout(10, Errno::ETIMEDOUT) do
        statistic_url = File.join(Settings.defaults.data_agent, "/api/visits")
        result = Tools.http_request(method: :post, url: statistic_url, params: {visits: visits, token: token})
      end
    rescue => e
      @@logger.error "创建访问记录失败：#{e}".red
    end
    return result
  end

  def pause_for_update
    pause_key = "fast_vps:pause_for_update"
    begin
      Timeout.timeout(60, Errno::ETIMEDOUT) do
        while ($redis.get pause_key)
          @@logger.warn "正在更新代码..."
          sleep 5
        end
      end
    rescue => e
      @@logger.warn "更新代码超时...".red
      @@logger.error e.message.red
    end
  end

end
