#encoding: utf-8
require 'rubygems'
require 'bundler/setup'
Bundler.require
require_relative 'settings'
require_relative 'tools'
require_relative 'click'
require_relative 'impression'

class PptpTask
  @@logger = Logger.new(File.expand_path('../../log/pptp_task.log', __FILE__), 3, 10485760)
  PATH = File.dirname(Pathname.new(__FILE__).realpath.to_path)
  VERSION = "1.3.24"

  def self.run
    count = 5
    pptp_task = PptpTask.new
    server_title = Socket.gethostname
    token = Digest::MD5::hexdigest("data_agent" + Time.now.strftime("%Y-%m-%d-%H"))

    # 1. 检测系统性能（ok）
    @@logger.warn "开始检测系统负载"
    return @@logger.warn "系统负载已满".red if Tools.check_performance
    @@logger.warn "系统负载检测通过".green

    # 2. 检测唯一进程（ok）
    @@logger.warn "开始检查是否是唯一进程"
    process_count = `ps aux|grep run_pptp_task|grep ruby |wc -l`
    @@logger.warn "当前进程数：#{process_count}"
    return @@logger.warn "不是唯一进程".red if process_count.to_i > 2
    @@logger.warn "唯一进程检测通过".green

    count.times do |i|
      begin
        @@logger.warn "开始第#{i+1}次任务"

        # 3. 关闭变换IP（ok）
        @@logger.warn "开始关闭pptp"
        `sudo poff myvpn`
        sleep 2
        @@logger.warn "关闭pptp成功".green

        # 4. 获取账户（ok）
        @@logger.warn "开始获取账户"
        get_account_result = pptp_task.get_account(server_title, token, VERSION)
        interval_time = get_account_result["account"]["interval_time"].to_i
        next @@logger.warn "获取账户失败".red if get_account_result["state"] == false
        @@logger.warn "获取账户成功".green

        # 5. 检查任务数量
        @@logger.warn "开始检查任务数量"
        task_count = pptp_task.get_task_count(server_title, get_account_result["account"]["city_title"], token, VERSION)
        next (@@logger.warn("任务数量为0".red) && sleep(5)) if task_count == 0
        @@logger.warn "检查任务数量成功".green

        # 6. 启动pptp
        @@logger.warn "开始启动pptp"
        begin_time = Time.now.to_i
        change_ip_result = pptp_task.change_ip(get_account_result["account"])
        if change_ip_result == false
          @@logger.warn "启动pptp失败".red
          task_time = Time.now.to_i - begin_time
          sleep (interval_time - task_time) if task_time < interval_time
          next
        end
        @@logger.warn "启动pptp成功".green

        # 7. 检测ip（ok）
        @@logger.warn "开始检测ip"
        check_ip_result = Tools.check_ip(@@logger, server_title, token)
        if check_ip_result == false
          @@logger.warn "检测ip失败".red
          `sudo poff myvpn`
          task_time = Time.now.to_i - begin_time
          sleep (interval_time - task_time) if task_time < interval_time
          next
        end
        @@logger.warn "检测ip成功".green

        Tools.check_performance_and_wait
        # 8. 检测网速（ok）
        @@logger.warn "开始检测网速"
        check_network_result = Tools.check_network(@@logger)
        if check_network_result == false
          @@logger.warn "检测网速失败".red
          `sudo poff myvpn`
          task_time = Time.now.to_i - begin_time
          sleep (interval_time - task_time) if task_time < interval_time
          next
        end
        @@logger.warn "检测网速成功".green

        # 9. 获取任务内容（ok）
        @@logger.warn "开始获取任务内容"
        get_task_result = pptp_task.get_task(server_title, get_account_result["account"]["city_title"], token, VERSION)
        next @@logger.warn "获取任务内容失败".red unless get_task_result["tasks"]
        @@logger.warn "获取任务内容成功，任务数量：#{get_task_result["tasks"].count}".green

        get_task_result["tasks"].each do |task|
          Tools.check_performance_and_wait
          # 10. 执行任务（ok）
          device_id_status = true
          if task["task_type"] == "click"#点击任务
            @@logger.warn "开始执行点击任务"
            Click.click(task, @@logger)
            @@logger.warn "执行点击任务成功".green
          elsif task["task_type"] == "impression"#曝光任务
            @@logger.warn "开始执行曝光任务"
            Impression.impression(task, @@logger)
            @@logger.warn "执行曝光任务成功".green
          elsif task["task_type"] == "click_with_device_id"#点击任务（带设备号）
            @@logger.warn "开始执行点击任务（带设备号）"
            device_id_status = Click.click(task, @@logger, true)
            @@logger.warn "执行点击任务（带设备号）#{device_id_status ? '成功' : '失败'}".green
          elsif task["task_type"] == "impression_with_device_id"#曝光任务（带设备号）
            @@logger.warn "开始执行曝光任务（带设备号）"
            device_id_status = Impression.impression(task, @@logger, true)
            @@logger.warn "执行曝光任务（带设备号）#{device_id_status ? '成功' : '失败'}".green
          else
            next
          end
          next unless device_id_status
        end

        # 11. 上传任务结果
        @@logger.warn "开始上传任务结果"
        visits = get_task_result["tasks"].collect{|task| {task_id: task["id"], useragent: task["useragent"]["content"], account_id: get_account_result["account"]["account_id"]}}
        pptp_task.create_visits(visits.to_json, token)
        @@logger.warn "上传任务结果成功".green

        task_time = Time.now.to_i - begin_time
        sleep (interval_time - task_time) if task_time < interval_time

        @@logger.warn "第#{i+1}次任务完成".green
      rescue => e
        `killall chrome`
        @@logger.warn "第#{i+1}次任务失败：#{e}".red
      end
    end

    # 12. 关闭变换IP
    @@logger.warn "开始关闭pptp"
    `sudo poff myvpn`
    sleep 2
    @@logger.warn "关闭pptp成功".green
  end

  def get_account(server_title, token, version)#ok
    result = {"state" => false}
    begin
      Timeout.timeout(10, Errno::ETIMEDOUT) do
        url = File.join(Settings.defaults.data_agent, "/api/pptp_accounts/show_by_task_client_title?task_client_title=#{server_title}&token=#{token}&task_client_version=#{version}")
        result = JSON.parse(open(url).read)
      end
    rescue => e
      @@logger.warn "获取账户失败：#{e}".red
      result = {}
    end
    return result
  end

  def get_task_count(server_title, city_title, token, version)
    result = {}
    begin
      Timeout.timeout(10, Errno::ETIMEDOUT) do
        url = File.join(Settings.defaults.data_agent, "/api/tasks/show_task_count?&city_title=#{city_title}&token=#{token}&account_type=pptp")
        url = Addressable::URI.parse(url).normalize
        result = JSON.parse(open(url).read, symbolize_names: true)
      end
    rescue => e
      @@logger.warn "获取任务失败：#{e}".red
    end
    return result[:task_count].to_i
  end

  def get_task(server_title, city_title, token, version)#ok
    result = {}
    begin
      Timeout.timeout(10, Errno::ETIMEDOUT) do
        url = File.join(Settings.defaults.data_agent, "/api/tasks?&city_title=#{city_title}&token=#{token}&account_type=pptp")
        url = Addressable::URI.parse(url).normalize
        result = JSON.parse(open(url).read)
      end
    rescue => e
      @@logger.warn "获取任务失败：#{e}".red
    end
    return result
  end

  def change_ip(account)#ok
    result = false
    begin
      Timeout.timeout(30, Errno::ETIMEDOUT) do
        #修改.sh
        `sudo poff myvpn`
        command = "source /home/change_ip.sh #{account["url"]} #{account["username"]} #{account["password"]}"
        system("bash -c #{command.inspect}")
        sleep (account["url"] =~ /\d+\.org/ ? 10 : 2)
        #开启pptp
        `sudo pon myvpn`
        20.times do |i|
          if `route -n`.include?("ppp0")
            result = true
            break
          end
          sleep 1
        end
        raise "pptp start failed" unless result
      end
    rescue => e
      @@logger.warn "启动pptp失败：#{e}".red
    end
    `sudo poff myvpn` if result == false
    return result
  end

  def create_visits(visits, token)
    result = nil
    begin
      Timeout.timeout(10, Errno::ETIMEDOUT) do
        statistic_url = File.join(Settings.defaults.data_agent, "/api/visits")
        result = Tools.http_request(method: :post, url: statistic_url, params: {visits: visits, token: token})
      end
    rescue => e
      @@logger.error "创建访问记录失败：#{e}".red
    end
    return result
  end

end
