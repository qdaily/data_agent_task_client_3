广告优化client端配置文档

##1.在client端安装vnc并进行ssh端口转发
```shell
ssh -C -N -f -g -L 5900:localhost:5902 data_agent@10.28.57.211
```
现在有服务器A和服务器B
服务器B跑pptp和拨号任务
服务器A当做跳板，好在服务器B跑任务的时候可以用vnc连接到服务器B
做配置如下：
服务器B上安装桌面环境
安装vnc服务并启动
```shell
apt-get install vnc4server
```
```shell
dpkg -l | grep vnc       #查看有无vnc安装包
```
配置vnc配置文件
```shell
vim ~/.vnc/xstartup
```
```shell
#!/bin/sh
exportXKL_XMODMAP_DISABLE=1
unsetSESSION_MANAGER
unsetDBUS_SESSION_BUS_ADDRESS
[-x/etc/vnc/xstartup]&&exec/etc/vnc/xstartup
[-r$HOME/.Xresources]&&xrdb$HOME/.Xresources
xsetroot-solidgrey
vncconfig-iconic&
gnome-session&
gnome-panel&
gnome-settings-daemon&
metacity&
nautilus&
gnome-terminal&
vncconfig-nowin&
```
给予可执行权限
```shell
sudo chmod +x ~/.vnc/xstartup
```
启动vnc服务
```shell
vncserver -geometry 1366x768 :1    #设置分辨率 :1的意思是在5901端口上跑，可以省略:1，系统会自动分配
```
kill vnc服务进程命令
```shell
vncserver -kill :1   #关闭5901端口上跑的vnc服务进程
```
至此服务器B的配置好了，在服务器A上设置ssh端口转发
```shell
ssh -C -N -f -g -L 5900:localhost:5902 data_agent@10.28.57.211
#ssh -C -N -f -g -L <本地端口>:<本地地址>:<远程端口> <远程用户名>@<远程地址>
```
这条命令将我们发往localhost:5900端口的数据转发到data_agent@10.28.57.211的5902端口上并把数据传回来。
也就是说我们在自己的电脑上向服务器A的5900端口发送数据，数据会转发到data_agent@10.28.57.211的5902端口上。
这样以来，只需要在自己的电脑上
```shell
vncviewer <服务器A的外网ip>:<端口号>
```
就能连接到服务器B的桌面上了

设置vncserver开机自动启动
新建/etc/init.d/vnc.sh文件
```shell
vim /etc/init.d/vnc.sh
```
写入如下内容
```shell
#!/bin/bash
echo "<密码>" | sudo su <用户> -c "/usr/bin/vncserver :1"
echo "<密码>" | sudo su <用户> -c "rm /var/www/data_agent_client_2/log/process.lock"

#!/bin/bash
PATH="$PATH:/usr/bin/"
export USER="data_agent"
#echo "Xy871116" | sudo -u data_agent /usr/bin/vncserver -geometry 1920x1080 :1
echo "Xy871116" | sudo su data_agent -c "/usr/bin/vncserver -geometry 1920x1080 :1"
##echo "Xy871116" | sudo su data_agent -c "rm /var/www/data_agent_client_2/log/process.lock"
```
保存退出，然后执行
```shell
update-rc.d vnc.sh defaults
```

##2.安装最新版的火狐浏览器并下载geckodriver
在这个地址https://github.com/mozilla/geckodriver/releases  
下载geckodriver然后放到/usr/local/bin目录下并把此文件权限改为755
```shell
sudo chmod 755 geckodriver
```

##3.修改shell脚本文件和相应pptp目录的权限
在/home目录底下的change_ip.sh
把该文件的权限改为755
```shell
sudo chmod 755 /home/change_ip.sh
```
/etc/ppp/chap-secrets文件权限改为777
```shell
sudo chmod 777 /etc/ppp/chap-secrets
```
/etc/ppp/peers目录权限改为777
```shell
sudo chmod 777 /etc/ppp/peers
```

##4.修改当前用户免密码使用pon，poff命令
修改/etc/sudoers文件
```shell
sudo vim /etc/sudoers
```
在最后一行加入
```shell
#设置pon poff git shutdown命令对于当前用户免密码执行
(当前用户) ALL = NOPASSWD:  /usr/bin/pon, /usr/bin/poff, /usr/bin/git, /sbin/shutdown
```
保存退出

##5.项目放到服务器的/var/www目录下

##6.配置定时任务
首先加载rvm环境到cron
```shell
rvm cron setup
```
然后
```shell
crontab -e
```
在其中写如以下内容
```shell
#每分钟执行一次run_data_agent_task
*/1 * * * * /bin/bash -l -c "cd /var/www/data_agent_client_2 ; rake run_data_agent_task" >> /var/www/data_agent_client_2/log/log.txt 2>&1

#每十分钟执行一次run_update_code拉取最新的代码
*/10 * * * * /bin/bash -l -c 'cd ~/data_agent_client_2 ; rake run_update_code' >> ~/data_agent_client_2/log/update_code_log.txt 2>&1

#每天凌晨两点执行一次run_restart_server，重启一次服务器
0 2 * * * /bin/bash -l -c 'cd ~/data_agent_client_2 ; rake run_restart_server' >> ~/data_agent_client_2/log/restart_server_log.txt 2>&1
```
定时任务的错误日志会输出到/var/www/data_agent_client_2/log目录下的对应的txt文件里

##7.给服务器分配虚拟内存
查看服务器内存情况
```shell
free -h #查看目前内存使用情况
```
建立swapfile文件
```shell
dd if=/dev/zero of=/mnt/swapfile bs=1M count=2048 # 建立swapfile文件，if表示input_file输入文件，of表示output_file输出文件，bs表示block_size块大小，count表示计数。这里，我采用了数据块大小为1M，数据块数目为2048，这样分配的空间就是2G大小。
```
格式化交换文件
```shell
mkswap /mnt/swapfile﻿​  #格式化交换文件
```
挂载交换文件
```shell
swapon /mnt/swapfile﻿​   #挂载交换文件
```
设置开机自动挂载swapfile，打开/etc/fstab
```shell
vim /etc/fstab
```
加入
```shell
/mnt/swapfile swap swap defaults 0 0﻿​  #设置开机自动挂载
```
以后每次开机都会挂载swapfile，实现分配虚拟内存
